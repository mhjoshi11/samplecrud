package unit.dao;

import static org.junit.Assert.*;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.Sample.SampleJDBC.JdbcConfig;
import com.Sample.SampleJDBC.dao.ProductDao;

public class ProductDeleteDaoTest {
	
	ApplicationContext context = new AnnotationConfigApplicationContext(JdbcConfig.class);
	ProductDao productDao = context.getBean("productDao", ProductDao.class);

	@Test
	public void test() {
		
		String pname = "Pen";
		
		assertEquals("Successfully deleted product in the table",1,productDao.delete(pname));
	}

}
