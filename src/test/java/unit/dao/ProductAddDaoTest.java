package unit.dao;

import static org.junit.Assert.*;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.Sample.SampleJDBC.JdbcConfig;
import com.Sample.SampleJDBC.dao.ProductDao;
import com.Sample.SampleJDBC.model.Product;

public class ProductAddDaoTest {
	
	ApplicationContext context = new AnnotationConfigApplicationContext(JdbcConfig.class);
	ProductDao productDao = context.getBean("productDao", ProductDao.class);

	@Test
	public void test() {
		Product product= new Product();
		
		product.setId(5);
		product.setName("Pen");
		product.setPrice(5);
		product.setQuantity(50);
		
		assertEquals("Successfully added Product into the Table",1,productDao.insert(product));
		
	}

}
