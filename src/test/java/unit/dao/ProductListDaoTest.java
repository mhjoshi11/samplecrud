package unit.dao;

import static org.junit.Assert.*;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.Sample.SampleJDBC.JdbcConfig;
import com.Sample.SampleJDBC.dao.ProductDao;

public class ProductListDaoTest {
	
	ApplicationContext context = new AnnotationConfigApplicationContext(JdbcConfig.class);
	ProductDao productDao = context.getBean("productDao", ProductDao.class);

	@Test
	public void test() {
				
		assertEquals("Successfully Fetched list of products from the table",4,productDao.getAllProducts().size());
	}

}
