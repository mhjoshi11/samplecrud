package com.Sample.SampleJDBC.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import com.Sample.SampleJDBC.model.Product;

@Component("ProductDao")
public class ProductDaoImpl implements ProductDao{
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	public int insert(Product product1) {
		String query="insert into product(id,name,price,quantity) values(?,?,?,?)";
		int r = this.jdbcTemplate.update(query,product1.getId(),product1.getName(),product1.getPrice(),product1.getQuantity());
		return r;
	}
	
	public int delete(String name) {
		String query ="delete from product where name=?";
		int r = this.jdbcTemplate.update(query,name);
		return r;
	}
	
	public List<Product> getAllProducts(){
		String query = "select * from product";
		RowMapper<Product> rowMapper = new RowMapperImpl();
		List<Product> products = this.jdbcTemplate.query(query, rowMapper);
		return products;
	}

	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

}
