package com.Sample.SampleJDBC.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.Sample.SampleJDBC.model.Product;

@Repository
public interface ProductDao {

	public int insert(Product product1);

	public int delete(String pname);

	public List<Product> getAllProducts();
	
	

}
