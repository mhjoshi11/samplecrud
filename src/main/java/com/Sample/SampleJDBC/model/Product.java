package com.Sample.SampleJDBC.model;

import lombok.Data;

@Data
public class Product {
	
	private int id;
	private String name;
	private float price;
	private int quantity;
}
