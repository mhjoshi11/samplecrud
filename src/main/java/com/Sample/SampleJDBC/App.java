package com.Sample.SampleJDBC;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Iterator;
import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;


import com.Sample.SampleJDBC.dao.ProductDao;
import com.Sample.SampleJDBC.model.Product;

public class App {
	public static void main(String[] args) {
		ApplicationContext context = new AnnotationConfigApplicationContext(JdbcConfig.class);
		ProductDao productDao = context.getBean("productDao", ProductDao.class);

		boolean flag = true;
		while (flag) {

			System.out.println("******----Welcome to Sample Project---******");
			System.out.println("Press 1 for add new Product");
			System.out.println("Press 2 for delete Product");
			System.out.println("Press 3 for display all Products");

			BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
			try {
				System.out.println("Enter your Choice: ");
				int n = Integer.parseInt(br.readLine());

				switch (n) {
				case 1: // add new Product
					System.out.println("Enter id: ");
					int id = Integer.parseInt(br.readLine());
					System.out.println("Enter Product Name: ");
					String name = br.readLine();
					System.out.println("Enter Product Price: ");
					float price = Float.parseFloat(br.readLine());
					System.out.println("Enter quantity: ");
					int qty = Integer.parseInt(br.readLine());

					Product product1 = new Product();
					product1.setId(id);
					product1.setName(name);
					product1.setPrice(price);
					product1.setQuantity(qty);

					int result = productDao.insert(product1);

					System.out.println(result + "Data inserted Successfully");
					break;
				case 2:// delete Product
					System.out.println("Enter Product Name: ");
					String pname = br.readLine();
					int r = productDao.delete(pname);
					if(r>0)
					System.out.println(r+" Success");
					else
						System.out.println("Error");
					break;
				case 3:// display all Products
					List<Product> prods = productDao.getAllProducts();
					System.out.println(prods);
//					System.out.println("PID  PNAME       PRICE    QUANTITY");
//					System.out.println("-------------------------------------------- ------");
//					for (Iterator iterator = prods.iterator(); iterator.hasNext();) {
//						Product product2 = (Product) iterator.next();
//						System.out.println(product2.getId() + "  " + product2.getName() + "  " + product2.getPrice()
//								+ "  " + product2.getQuantity());
//						System.out.println("-------------------------------------------- ------");
//					}
					break;
				case 4:// exit
					flag = false;
					break;

				default:
					break;
				}
			} catch (NumberFormatException e) {
				System.out.println("Type valid number");
				e.printStackTrace();
			} catch (IOException e) {
				System.out.println("Invalid Input");
				e.printStackTrace();
			}

		}

	}
}
