package com.Sample.SampleJDBC;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import com.Sample.SampleJDBC.dao.ProductDao;
import com.Sample.SampleJDBC.dao.ProductDaoImpl;
import com.Sample.SampleJDBC.model.Product;

@Configuration
@ComponentScan(basePackages = {"com.Sample.SampleJDBC.dao"})
public class JdbcConfig {
	
	/**
	 * @return
	 */
	@Bean(name = { "ds" })
	public DataSource getDataSource() {
		DriverManagerDataSource ds = new DriverManagerDataSource();
		ds.setDriverClassName("com.mysql.cj.jdbc.Driver");
		ds.setUrl("jdbc:mysql://localhost:3306/crud");
		ds.setUsername("root");
		ds.setPassword("maya123");
		return ds;
	}
	
	@Bean(name = { "jdbcTemplate" })
	public JdbcTemplate getTemplate() {
		JdbcTemplate jdbcTemplate = new JdbcTemplate();
		jdbcTemplate.setDataSource(getDataSource()); 
		return jdbcTemplate;
	}
	@Bean(name = { "productDao" })
	public ProductDao getProductDao() {
		ProductDaoImpl productDao = new ProductDaoImpl();
		productDao.setJdbcTemplate(getTemplate());
		return productDao;
	}
	
}
